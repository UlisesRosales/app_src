import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class AsistenciaService {
  ruta: string = 'https://fer-sepulveda.cl/API_PRUEBA_3/api-service.php';


  constructor(private http: HttpClient) { }

  aistenciaAlmacenar(usuario: string, asignatura: string, seccion: string, fecha: string) {
    return this.http.post(this.ruta, {
      nombreFuncion: "AsistenciaAlmacenar",
      parametros: [
        usuario, asignatura, seccion, fecha
      ]
    }).pipe();

  }
  asistenciaObtener(usuario: string){
    return this.http.get(this.ruta + '?nombreFuncion=AsistenciaObtener&usuario=' + usuario).pipe();
}

}
