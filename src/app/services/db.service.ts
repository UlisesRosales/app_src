import { Injectable } from '@angular/core';
import { SQLite, SQLiteObject } from '@awesome-cordova-plugins/sqlite/ngx';


@Injectable({
  providedIn: 'root'
})
export class DbService {
  closeDatabase() {
    throw new Error('Method not implemented.');
  }


  constructor(private sqlite: SQLite) {
    /*this.sqlite.create({
      name: 'data.db',
      location: 'default'
    })
      .then((db: SQLiteObject) => {
        db.executeSql('DROP TABLE IF EXISTS USUARIO', [])
          .then(() => console.log('FSR: TABLA ELIMINADA OK'))
          .catch(e => console.log('FSR: ' + JSON.stringify(e)));
      })
      .catch(e => console.log('FSR: ' + JSON.stringify(e)));*/
  


  
    this.sqlite.create({
      name: 'data.db',

      location: 'default'
    })
      .then((db: SQLiteObject) => {
        db.executeSql('CREATE TABLE IF NOT EXISTS USUARIO (nombreUsuario VARCHAR(20), correoElectronico VARCHAR(20), contrasena VARCHAR(20),nombre VARCHAR(20),apellido VARCHAR(30))', [])
          .then(() => console.log('FPC: TABLA CREADA OK'))
          .catch(e => console.log('FPC: ' + JSON.stringify(e)));
      })
      .catch(e => console.log('FPC: ' + JSON.stringify(e)));
    this.sqlite.create({
      name: 'data.db',

      location: 'default'
    })
      .then((db: SQLiteObject) => {
        db.executeSql('CREATE TABLE IF NOT EXISTS sesion (nombreUsuario VARCHAR(20),  contrasena VARCHAR(20))', [])
          .then(() => console.log('FPC: TABLA sesion CREADA OK'))
          .catch(e => console.log('FPC: error al crear tabla ' + JSON.stringify(e)));
      })
      .catch(e => console.log('FPC: error ' + JSON.stringify(e)));


  }


  getUsuario(nombreUsuario: string, contrasena: string) {
    return this.sqlite.create({
      name: 'data.db',
      location: 'default'
    })
      .then((db: SQLiteObject) => {
        return db.executeSql('SELECT COUNT(nombreUsuario)AS cantidad FROM  USUARIO WHERE nombreUsuario = ? AND contrasena = ? ', [nombreUsuario, contrasena])
          .then((data) => {
            return data.rows.item(0).cantidad;
          })
          .catch(e => console.log('FPC: error' + JSON.stringify(e)));
      })
      .catch(e => console.log('FPC: error' + JSON.stringify(e)));
  }





  agregarUsuario(nombreUsuario: string, correoElectronico: string, contrasena: string, nombre: string, apellido: string): Promise<void> {
    console.log('Agregando usuario...');
    return new Promise((resolve, reject) => {
      this.sqlite.create({
        name: 'data.db',
        location: 'default'
      }).then((db: SQLiteObject) => {
        db.executeSql('SELECT * FROM USUARIO WHERE correoElectronico = ? OR nombreUsuario = ?', [correoElectronico, nombreUsuario])
          .then((data) => {
            if (data.rows.length > 0) {
              console.log('FPC El correo electrónico o el nombre de usuario ya están en uso.');
              reject('FPC El correo electrónico o el nombre de usuario ya están en uso.');
            } else {
              db.executeSql('INSERT INTO USUARIO VALUES(?, ?, ?, ?, ?)', [nombreUsuario, correoElectronico, contrasena, nombre, apellido])
                .then(() => {
                  console.log('FPC: USUARIO ALMACENADO OK');
                  db.close()
                    .then(() => {
                      console.log('FPC Base de datos cerrada.');
                      resolve();
                    })
                    .catch(e => {
                      console.error('FPC Error al cerrar la base de datos: ' + JSON.stringify(e));
                      reject(e);
                    });
                }).catch(e => {
                  console.error('FPC: Error al almacenar usuario: ' + JSON.stringify(e));
                  reject(e);
                });
            }
          }).catch(e => {
            console.error('FPC Error al verificar correo electrónico o nombre de usuario: ' + JSON.stringify(e));
            reject(e);
          });
      }).catch(e => {
        console.error('FPC: Error al abrir la base de datos: ' + JSON.stringify(e));
        reject(e);
      });
    });
  }



  async actualizarUsuarioPorNombre(nombreUsuario: string, nuevaContrasena: string): Promise<boolean> {
    return new Promise<boolean>((resolve) => {
      this.sqlite.create({
        name: 'data.db',
        location: 'default'
      }).then((db: SQLiteObject) => {
        // Primero, verifica si el usuario existe
        db.executeSql('SELECT * FROM USUARIO WHERE nombreUsuario = ?', [nombreUsuario])
          .then((data) => {
            if (data.rows.length > 0) {
              // Si se encontró al menos un usuario con el nombre de usuario dado
              // Puedes proceder a actualizar su contraseña
              db.executeSql('UPDATE USUARIO SET contrasena = ? WHERE nombreUsuario = ?', [nuevaContrasena, nombreUsuario])
                .then(() => {
                  console.log('FPC Contraseña actualizada con éxito en la base de datos.');
                  resolve(true); // Resolvemos la promesa con `true` si la actualización tiene éxito
                })
                .catch(e => {
                  console.log('FPC: ' + JSON.stringify(e));
                  resolve(false); // Resolvemos la promesa con `false` si la actualización falla
                });
            } else {
              console.log('FPC El usuario con el nombre de usuario proporcionado no existe.');
              resolve(false); // Resolvemos la promesa con `false` si el usuario no se encuentra
            }
          })
          .catch(e => {
            console.log('FPC: ' + JSON.stringify(e));
            resolve(false); // Resolvemos la promesa con `false` si ocurre un error
          });
      }).catch(e => {
        console.log('FPC: ' + JSON.stringify(e));
        resolve(false); // Resolvemos la promesa con `false` si ocurre un error al abrir la base de datos
      });
    });
  }



  obtenerCantidadUsuarios() {
    return this.sqlite.create({
      name: 'data.db',
      location: 'default'
    })
      .then((db: SQLiteObject) => {
        return db.executeSql('SELECT COUNT(nombreUsuario) AS CANTIDAD FROM USUARIO', [])
          .then((data) => {
            return data.rows.item(0).CANTIDAD;
          })
          .catch(e => console.log('FPC: ' + JSON.stringify(e)));
      })
      .catch(e => console.log('FSR: ' + JSON.stringify(e)));
  }


  infoUsuario(nombreUsuario: string, contrasena: string) {
    return this.sqlite.create({
      name: 'data.db',
      location: 'default'
    })
      .then((db: SQLiteObject) => {
        return db.executeSql('SELECT correoElectronico, nombre,apellido FROM USUARIO where nombreUsuario = ? and contrasena = ? ', [nombreUsuario, contrasena])
          .then((data) => {
            let objeto: any = {};
            objeto.nombre = data.rows.item(0).nombre;
            objeto.correoElectronico = data.rows.item(0).correoElectronico;
            objeto.apellido = data.rows.item(0).apellido;
            return objeto;
          })
          .catch(e => console.log('FPC: ' + JSON.stringify(e)));
      })
      .catch(e => console.log('FPC: ' + JSON.stringify(e)));
  }


  almacenarSesion(nombreUsuario: string, contrasena: string) {
    this.sqlite.create({
      name: 'data.db',
      location: 'default'
    })
      .then((db: SQLiteObject) => {
        db.executeSql('insert into sesion values(?, ?) ', [nombreUsuario, contrasena])
          .then(() => console.log('FPC: sesion almacenada ok'))
          .catch(e => console.log('FPC: error al almacenar  ' + JSON.stringify(e)));
      })
      .catch(e => console.log('FPC: error' + JSON.stringify(e)));
  }

  validarSesion() {
    return this.sqlite.create({
      name: 'data.db',
      location: 'default'
    })
      .then((db: SQLiteObject) => {
        return db.executeSql('SELECT COUNT(nombreUsuario)AS cantidad FROM  sesion ', [])
          .then((data) => {
            return data.rows.item(0).cantidad;
          })
          .catch(e => console.log('FPC: error sesion'));
      })
      .catch(e => console.log('FPC: error' + JSON.stringify(e)));
  }

  eliminarSesion() {
    this.sqlite.create({
      name: 'data.db',
      location: 'default'
    })
      .then((db: SQLiteObject) => {
        db.executeSql('delete from sesion ', [])
          .then(() => console.log('FPC: sesion eliminada ok'))
          .catch(e => console.log('FPC: error al eliminar  ' + JSON.stringify(e)));
      })
      .catch(e => console.log('FPC: error' + JSON.stringify(e)));
  }
  async obtenerSesion() {
    try {
      const db = await this.sqlite.create({
        name: 'data.db',
        location: 'default'
      });
      const data = await db.executeSql('SELECT nombreUsuario, contrasena FROM sesion', []);
      if (data.rows.length > 0) {
        const objeto: any = {
          nombreUsuario: data.rows.item(0).nombreUsuario,
          contrasena: data.rows.item(0).contrasena
        };
        return objeto;
      } else {
        return null; 
      }
    } catch (error) {
      console.error('FPC: error al obtener sesión', error);
      throw error; 
    }
  }
}  