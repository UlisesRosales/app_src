import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'

})
export class UsuarioService {
  ruta: string = 'https://fer-sepulveda.cl/API_PRUEBA_2/api-service.php';



  constructor(private router: Router, private http: HttpClient ) { }


  almacenarUsuario(nombreUsuario: string, correoElectronico: string, contrasena: string, nombre: string, apellido: string) {
    console.log('Realizando solicitud para almacenar usuario...');
  
    return this.http.post(this.ruta, {
      nombreFuncion: 'UsuarioAlmacenar',
      parametros: [nombreUsuario, correoElectronico, contrasena, nombre, apellido]
    }).pipe();
  }

  usuarioLogin(nombreUsuario: string, contrasena: string) {
    console.log('Realizando solicitud para iniciar sesión de usuario...');
    return this.http.post(this.ruta, {
      nombreFuncion: 'UsuarioLogin',
      parametros: [nombreUsuario, contrasena]
    }).pipe();
  }

  usuarioModificarContrasena(nombreUsuario: string, contrasenaNuevo: string, contrasenaActual: string) {
    console.log('Realizando solicitud para modificar contraseña de usuario...');
    return this.http.patch(this.ruta, {
      nombreFuncion: 'UsuarioModificarContrasena',
      parametros: [nombreUsuario, contrasenaNuevo, contrasenaActual]
    }).pipe();
  }
  
}
