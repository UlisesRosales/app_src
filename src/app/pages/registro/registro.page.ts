import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UsuarioService } from 'src/app/services/usuario.service';
import { firstValueFrom } from 'rxjs';
import { DbService } from 'src/app/services/db.service';
import { Camera, CameraResultType } from '@capacitor/camera';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.page.html',
  styleUrls: ['./registro.page.scss'],
})
export class RegistroPage implements OnInit {
  nombreUsuario: string = '';
  correoElectronico: string = '';
  contrasena: string = '';
  confirmarContrasena: string = '';
  nombre: string = '';
  apellido: string = '';

  isAlertOpen: boolean = false;
  alertHeader: string = '';
  alertMessage: string = '';
  cantidad: number = 0;

  constructor(private usuarioService: UsuarioService, private router: Router, private dbService: DbService) { }
  ruta = '';
  ngOnInit() {
  }
  ; async tomarFoto() {
    const image = await Camera.getPhoto({
      quality: 90,
      allowEditing: true,
      resultType: CameraResultType.Uri
    });


    var imageUrl = image.webPath;
    if (imageUrl)
      this.ruta = imageUrl;
  };
  navegarLogin() {
    this.router.navigate(['/login']);
  }
  async registro() {
    if (!this.nombreUsuario || !this.correoElectronico || !this.contrasena || !this.confirmarContrasena|| !this.nombre|| !this.apellido) {
      this.mostrarAlerta('Campos incompletos', 'Por favor, complete todos los campos.');
      return;
    }

    if (this.contrasena.length < 8) {
      this.mostrarAlerta('Contraseña débil', 'La contraseña debe tener al menos 8 caracteres.');
      return;
    }

    const emailRegex = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/;
    if (!emailRegex.test(this.correoElectronico)) {
      this.mostrarAlerta('Correo electrónico no válido', 'Ingrese una dirección de correo electrónico válida.');
      return;
    }

    try {
      
      await this.dbService.agregarUsuario(this.nombreUsuario, this.correoElectronico, this.contrasena, this.nombre, this.apellido);
      let data = this.usuarioService.almacenarUsuario(this.nombreUsuario, this.correoElectronico, this.contrasena, this.nombre, this.apellido);
      let respuesta = await firstValueFrom(data);

      if (respuesta = {"result":[{"RESPUESTA":"OK"}]})
      {
        setTimeout(() => {
          this.limpiarCampos();
        this.navegarLogin();
        }, 3000); 
        
        console.log('Usuario almacenado correctamente en la API y en la base de datos local.');
        this.mostrarAlerta('Usuario almacenado correctamente', 'El usuario ha sido almacenado correctamente.');
        console.log('Datos almacenados con éxito en la base de datos.');
        console.error('Error al almacenar usuario en la API: '+ respuesta);
        if (respuesta ={"result":[{"RESPUESTA":"ERR01"}]}) {
        } else if (respuesta ={"result":[{"RESPUESTA":"ERR02"}]}) {
          // Correo ya existe
          this.mostrarAlerta('Correo ya existente', 'El correo ya existe en la base de datos.');
        } else {
          console.log(respuesta)
          this.mostrarAlerta('Error al almacenar usuario', 'Ocurrió un error al almacenar el usuario en la API. ' );
        }
      }
    } catch (error) {
      console.error('Error al almacenar usuario: ' + error);
      this.mostrarAlerta('Error al almacenar usuario', 'Ocurrió un error al almacenar el usuario en la base de datos local.');
    }
  }

  async obtenerCantidadUsuarios() {
    try {
      const data = await this.dbService.obtenerCantidadUsuarios();
      this.cantidad = data;
    
      console.log('FRC Datos almacenados con éxito en la base de datos.');

      this.nombreUsuario = '';
      this.correoElectronico = '';
      this.contrasena = '';
      this.confirmarContrasena = '';
      
    } catch (error) {
      console.error('FRC Error en la consulta a la base de datos: ' + error);
    }
  }

  limpiarCampos() {
    this.nombreUsuario = '';
    this.correoElectronico = '';
    this.contrasena = '';
    this.confirmarContrasena = '';
    this.nombre = '';
    this.apellido = '';
  }

  mostrarAlerta(header: string, message: string) {
    this.isAlertOpen = true;
    this.alertHeader = header;
    this.alertMessage = message;
  }
}
