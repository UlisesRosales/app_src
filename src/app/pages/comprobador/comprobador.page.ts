import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DbService } from 'src/app/services/db.service';


@Component({
  selector: 'app-comprobador',
  templateUrl: './comprobador.page.html',
  styleUrls: ['./comprobador.page.scss'],
})
export class ComprobadorPage implements OnInit {

  constructor(private router: Router, private dbService: DbService) { }

  ngOnInit() {
    setTimeout(() => {
      this.dbService.validarSesion().then(data => {
        console.log('FPR Resultado de validarSesion:'+ data);
        if (data === 0) {
          this.router.navigate(['login']);
        } else {
          this.router.navigate(['principal']);
        }
      });
    }, 2000);
  }



}
