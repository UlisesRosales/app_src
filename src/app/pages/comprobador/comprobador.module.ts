import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ComprobadorPageRoutingModule } from './comprobador-routing.module';

import { ComprobadorPage } from './comprobador.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComprobadorPageRoutingModule
  ],
  declarations: [ComprobadorPage]
})
export class ComprobadorPageModule {}
