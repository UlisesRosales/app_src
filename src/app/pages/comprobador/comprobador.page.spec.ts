import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ComprobadorPage } from './comprobador.page';

describe('ComprobadorPage', () => {
  let component: ComprobadorPage;
  let fixture: ComponentFixture<ComprobadorPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(ComprobadorPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
