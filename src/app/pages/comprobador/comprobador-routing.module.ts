import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ComprobadorPage } from './comprobador.page';

const routes: Routes = [
  {
    path: '',
    component: ComprobadorPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ComprobadorPageRoutingModule {}
