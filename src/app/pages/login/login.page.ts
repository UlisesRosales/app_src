import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { lastValueFrom } from 'rxjs';
import { DbService } from 'src/app/services/db.service';
import { UsuarioService } from 'src/app/services/usuario.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  mdl_usuario: string = '';
  mdl_contrasena: string = '';
  isAlertOpen: boolean = false;
  alertHeader: string = '';
  alertMessage: string = '';
  cantidad: number = 0;

  constructor(private router: Router, private dbService: DbService, private usuarioService: UsuarioService) { }

  ngOnInit() {


    this.obtenerCantidadUsuarios();
  }

  navegarRecuperar() {
    this.router.navigate(['/recuperar']);
    this.limpiarCampos();
  }
  navegarARegistro() {
    this.router.navigate(['/registro']);
    this.limpiarCampos();
  }
  async obtenerCantidadUsuarios() {
    try {
      this.cantidad = await this.dbService.obtenerCantidadUsuarios();
      console.log('FPC Cantidad de usuarios obtenida con éxito: ' + this.cantidad);

    } catch (error) {
      console.error(' FPC Error al obtener la cantidad de usuarios: ' + error);
    }
  }

  async navegar() {
    if (!this.mdl_usuario || !this.mdl_contrasena) {

      this.mostrarAlerta('Error', 'Por favor, complete todos los campos.');
      return;
    }

    try {
      const data = await this.dbService.getUsuario(this.mdl_usuario, this.mdl_contrasena);

      if (data == 1) {
        this.mostrarAlerta('Ingreso exitoso', 'Bienvenido, estás iniciando sesión...');
        this.dbService.almacenarSesion(this.mdl_usuario, this.mdl_contrasena);
        let extras: NavigationExtras = {
          replaceUrl: true,
          state: {
            usuario: this.mdl_usuario,
            contrasena: this.mdl_contrasena
          }

        }
        this.router.navigate(['principal'], extras);
        setTimeout(() => {
          let extras: NavigationExtras = {
            replaceUrl: true,
            state: {
              usuario: this.mdl_usuario,
              contrasena: this.mdl_contrasena
            }

          }
          this.router.navigate(['principal'], extras);
          this.login();
          this.limpiarCampos();
        }, 2000);
      } else {
        this.login();
        console.log('FPC Credenciales inválidas');
        this.mostrarAlerta('Error', 'Credenciales inválidas');
      }
    } catch (error) {
      console.error('FPC Error al obtener el usuario desde la base de datos: ', error);

      this.mostrarAlerta('FPC Error', 'Ocurrió un error al intentar iniciar sesión. Inténtalo de nuevo más tarde.');
    }
  }

  async login() {
    if (!this.mdl_usuario || !this.mdl_contrasena) {
      this.mostrarAlerta('Error', 'Por favor, ingrese tanto el nombre de usuario como la contraseña.');
      return;
    }

    try {
      let data = await this.usuarioService.usuarioLogin(this.mdl_usuario, this.mdl_contrasena);
      let respuesta = await lastValueFrom(data);
      let usuario = JSON.stringify(respuesta);
      let json = JSON.parse(usuario);

      if (json.result && json.result[0].RESPUESTA === "LOGIN OK") {
        this.dbService.almacenarSesion(this.mdl_usuario, this.mdl_contrasena);
        console.log(JSON.stringify(this.mdl_usuario));
        let extras: NavigationExtras = {
          replaceUrl: true,
          state: {
            usuario: this.mdl_usuario,
            contrasena: this.mdl_contrasena
          }

        }
        this.router.navigate(['principal'], extras);
        this.limpiarCampos();

      } else if (json.result && json.result[0].RESPUESTA === "LOGIN NOK") {
        console.log(usuario);
        this.mostrarAlerta('Error', 'Credenciales inválidas. Por favor, inténtelo nuevamente.');
      } else {
        console.log(usuario);
        this.mostrarAlerta('Error', 'Respuesta inesperada de la API');
      }
    } catch (error) {
      console.error("Error al iniciar sesión:" + error);
      this.mostrarAlerta('Error', 'Error al iniciar sesión. Por favor, inténtelo nuevamente.');
    }
  }

  limpiarCampos() {
    this.mdl_usuario = '';
    this.mdl_contrasena = '';
  }

  mostrarAlerta(header: string, message: string) {
    this.isAlertOpen = true;
    this.alertHeader = header;
    this.alertMessage = message;
  }

  setOpen(isOpen: boolean) {
    this.isAlertOpen = isOpen;
  }
}







