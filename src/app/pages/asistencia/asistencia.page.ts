import { Component, OnInit } from '@angular/core';
import { AsistenciaService } from 'src/app/services/asistencia.service';
import { lastValueFrom } from 'rxjs';
import { Router } from '@angular/router';
import { DbService } from 'src/app/services/db.service';

@Component({
  selector: 'app-asistencia',
  templateUrl: './asistencia.page.html',
  styleUrls: ['./asistencia.page.scss'],
})
export class AsistenciaPage implements OnInit {
  usuario: string = '';
  asistencias: any[] = [];
  lista: any[] = [];


  constructor(private asistencia: AsistenciaService, private router: Router, private dbService: DbService) { }
  ngOnInit() {
    let extras = this.router.getCurrentNavigation();
    if (extras?.extras.state) {
      this.usuario = extras?.extras.state['usuario'];
    }
    console.log('FRC Usuario:' + JSON.stringify(this.usuario));
    if (this.usuario == '') {
      this.dbService.obtenerSesion().then(data => {
        this.usuario = data.nombreUsuario;
      });
    }
  }
  principal() {
    this.router.navigate(['principal']);
  }
 /* async mostrarAsistencia() {
    console.log('estoy aqui')
    console.log(JSON.stringify(this.usuario));
    try {
      let data = await this.asistencia.asistenciaObtener(this.usuario);
      let respuesta = await lastValueFrom(data);
      let json = respuesta;

      {
        json = {
          "result": [
            {
              "ASIGNATURA": "",
              "CODIGO_ASIGNATURA": "",
              "TOTAL_CLASES": "",
              "CLASES_PRESENTE": "",
              "CLASES_AUSENTE": "",
              "USUARIO": this.usuario
            }
          ]
        };
      }
      console.log(JSON.stringify(json))
    } catch (error) {
      console.error('Error al obtener la asistencia:' + error);
    }
  }*/
  async mostrarAsistencia() {
    try {
      this.asistencias = [];
      const respuesta = await lastValueFrom(this.asistencia.asistenciaObtener(this.usuario));
      let json = JSON.stringify(respuesta);
      let datos = JSON.parse(json);
      for (let x = 0; x < datos['result'].length; x++) {
        this.asistencias.push(datos['result'][x]);
      }
    } catch (error) {
      console.error('Error en mostrarAsistencia:'+ error);
    }
  }
  
}
