import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Barcode, BarcodeScanner } from '@capacitor-mlkit/barcode-scanning';
import { AlertController } from '@ionic/angular';
import { AsistenciaService } from 'src/app/services/asistencia.service';
import { DbService } from 'src/app/services/db.service';
import { lastValueFrom } from 'rxjs';
@Component({
  selector: 'app-principal',
  templateUrl: './principal.page.html',
  styleUrls: ['./principal.page.scss'],
})
export class PrincipalPage implements OnInit {

  usuario: string = '';
  contrasena: string = '';

  nombre: string = '';
  apellido: string = '';
  correo: string = '';

  color: string = 'light';

  isSupported = false;
  barcodes: Barcode[] = [];
  separatedData: any[] = [];

  asignatura: string = '';
  seccion: string = '';
  fecha: string = '';


  isAlertOpen: boolean = false;
  alertHeader: string = '';
  alertMessage: string = '';

  constructor(private router: Router, private dbService: DbService, private asistencia: AsistenciaService, private alertController: AlertController) { }


  ngOnInit() {
    let extras = this.router.getCurrentNavigation();
    if (extras?.extras.state) {
      this.usuario = extras?.extras.state['usuario'];
      this.contrasena = extras?.extras.state['contrasena'];

      this.infoUsuario();

    }
    console.log('FRC Usuario:' + JSON.stringify(this.usuario));
    console.log('FRC Contraseña:' + JSON.stringify(this.contrasena));
    if (this.usuario == '') {
      this.dbService.obtenerSesion().then(data => {
        this.usuario = data.nombreUsuario;
        this.contrasena = data.contrasena;
        this.infoUsuario();

      });
      console.log('Info Usuario:' + JSON.stringify(this.infoUsuario));
    } else {
      this.infoUsuario();

    }

    BarcodeScanner.isSupported().then((result) => {
      this.isSupported = result.supported;
      console.log('Barcode Scanner Supported:' + this.isSupported);
    });
  }


  ionViewWillEnter() {
    this.barcodes = []; 
  }

  async scan(): Promise<void> {
    try {
      const ress = await BarcodeScanner.isGoogleBarcodeScannerModuleAvailable();
  
      if (!ress.available) {
        await BarcodeScanner.installGoogleBarcodeScannerModule();
      }
  
      const granted = await this.requestPermissions();
  
      if (!granted) {
        this.presentAlert();
        return;
      }
  
      const { barcodes } = await BarcodeScanner.scan();
      this.barcodes = [];
      this.barcodes.push(...barcodes);
      console.log('Datos del código QR:', JSON.stringify(barcodes));
  
      for (const barcode of barcodes) {
        const { displayValue } = barcode;
        const [code, subject, room, date] = displayValue.split('|');
        const codigoSesion = code.split('-')[1];
        const codigoAsignatura = code.split('-')[0];
  
        console.log('Codigo de asignatura:', code);
        console.log('Nombre de materia:', subject);
        console.log('Número de sala:', room);
        console.log('Fecha:', date);
        console.log('Código de Sesion:', codigoSesion);
        console.log('Código de asignatura:', codigoAsignatura);
  
        let respuesta = this.asistencia.aistenciaAlmacenar(this.usuario, codigoAsignatura, codigoSesion, date);
        let data = await lastValueFrom(respuesta);
        let json = JSON.stringify(data);
        console.log(json);
  
        if (json === '{"result":[{"RESPUESTA":"ASISTENCIA_OK"}]}') {
          console.log('Asistencia marcada exitosamente', JSON.stringify(respuesta));
          this.mostrarAlerta('exito', 'Asistencia marcada exitosamente');
        } else if (json === '{"result":[{"RESPUESTA":"ASISTENCIA_NOK"}]}') {
          this.mostrarAlerta('Error', 'Ya se encuentra presente en esta clase');
          console.log('Ya se encuentra presente en esta clase');
        } else {
          console.log('Respuesta inesperada de la API', JSON.stringify(respuesta));
        }
      }
    } catch (error) {
      console.error('Error durante el escaneo:', error);
      // Puedes mostrar un mensaje de error al usuario si lo deseas
      this.mostrarAlerta('Error', 'Hubo un error durante el escaneo del código QR');
    }
  }
  

  async requestPermissions(): Promise<boolean> {
    const { camera } = await BarcodeScanner.requestPermissions();
    return camera === 'granted' || camera === 'limited';
  }

  async presentAlert(): Promise<void> {
    const alert = await this.alertController.create({
      header: 'Permission denied',
      message: 'Please grant camera permission to use the barcode scanner.',
      buttons: ['OK'],
    });
    await alert.present();
  }





  mostrarAlerta(header: string, message: string) {
    this.isAlertOpen = true;
    this.alertHeader = header;
    this.alertMessage = message;
  }



















  infoUsuario() {
    console.log('Antes de llamar a infoUsuario');
    console.log('Usuario:' + this.usuario);
    console.log('Contraseña:' + this.contrasena);

    this.dbService.infoUsuario(this.usuario, this.contrasena)
      .then(data => {
        console.log('Después de llamar a infoUsuario. Datos del usuario:' + JSON.stringify(data));

        if (data) {
          console.log('Datos del usuario:' + JSON.stringify(data));
          this.correo = data.correoElectronico;
          this.nombre = data.nombre;
          this.apellido = data.apellido;
        } else {
          console.log('Datos del usuario no disponibles. Mostrando solo el usuario:' + JSON.stringify(this.usuario) || 'No hay usuario definido');
        }
      })
      .catch(error => {
        console.error('FPC Error al obtener datos del usuario:' + error);
      });
  }




  fav() {
    if (this.color == 'light') {
      this.color = 'danger';
    } else {
      this.color = 'light';
    }
  }

}
