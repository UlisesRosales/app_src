import { Component } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { DbService } from 'src/app/services/db.service';

@Component({
    selector: 'menu',
    templateUrl: './menu.componente.html',
    styleUrls: ['./menu.componente.css']
})
export class MenuComponent {

    constructor(private router: Router,private dbService: DbService) { }

    recuperar() {
        this.router.navigate(['/recuperar']);
    }
    asistencia(){
        this.router.navigate(['/asistencia']);
    }
    cerrarSesion() {
this.dbService.eliminarSesion();
let extras: NavigationExtras = {
    replaceUrl: true
}
this.router.navigate(['login'],extras);
    }
}