import { Component } from '@angular/core';
import { UsuarioService } from 'src/app/services/usuario.service';
import { Router } from '@angular/router';
import { DbService } from 'src/app/services/db.service';
import { firstValueFrom } from 'rxjs';

@Component({
  selector: 'app-recuperar',
  templateUrl: './recuperar.page.html',
  styleUrls: ['./recuperar.page.scss'],
})
export class RecuperarPage {
  usuarioIngresado: boolean = false;
  nombreUsuario: string = '';
  nuevaContrasena: string = '';
  contrasenaActual: string = '';
  ConfirmarContrasena: string = '';
  isAlertOpen: boolean = false;
  alertHeader: string = '';
  alertMessage: string = '';

  constructor(private usuarioService: UsuarioService, private dbService: DbService, private router: Router) { }
  navegarLogin() {
    this.router.navigate(['/login']);
    this.limpiarCampos();
  }
  async recuperarContrasena() {
    if (this.nombreUsuario === '' || this.contrasenaActual === '' || this.nuevaContrasena === ''||this.ConfirmarContrasena==='') {
      this.mostrarAlerta('Campos incompletos', 'Por favor, complete todos los campos.');
      return;
    }
    if (this.nuevaContrasena !== this.ConfirmarContrasena) {
      this.mostrarAlerta('Contraseñas no coinciden', 'La nueva contraseña y la confirmación no son iguales.');
      return;
    }
    try {
      
      let data = this.usuarioService.usuarioModificarContrasena(this.nombreUsuario, this.nuevaContrasena, this.contrasenaActual);
      let respuesta = await firstValueFrom(data);
      console.log('Respuesta de la API:'+JSON.stringify(respuesta) );
  
      if (respuesta ={"result":[{"RESPUESTA":"OK"}]}) {
        const exitoLocal = await this.dbService.actualizarUsuarioPorNombre(this.nombreUsuario, this.nuevaContrasena);
  
        console.log('Éxito en la base de datos local:', respuesta);
  
        if (exitoLocal) {
          this.mostrarAlerta('Contraseña actualizada', 'La contraseña se ha actualizado con éxito.');
          this.isAlertOpen = true;
          setTimeout(() => {
            this.router.navigate(['principal']);
            this.limpiarCampos();
          }, 3000); 
        } else {
          this.mostrarAlerta('Error en la base de datos', 'Ha ocurrido un error al actualizar la contraseña en la base de datos local.');
        }
      } else if (respuesta ={"result":[{"RESPUESTA":"LOGIN NOK"}]}) {
        this.mostrarAlerta('Error en la API', 'Ha ocurrido un error al actualizar la contraseña en la API .');
      } else {
        this.mostrarAlerta('Error en la API', 'Ha ocurrido un error al actualizar la contraseña en la API (otro error).');
      }
    } catch (error) {
      console.error('Error al recuperar la contraseña: ' + error);
      this.mostrarAlerta('Error', 'Ha ocurrido un error al recuperar la contraseña.');
    }
  }
  
  limpiarCampos() {
    this.nombreUsuario = '';
    this.nuevaContrasena = '';
    this.contrasenaActual = '';
    this.ConfirmarContrasena = '';
    
  }

  mostrarAlerta(header: string, message: string) {
    this.isAlertOpen = true;
    this.alertHeader = header;
    this.alertMessage = message;
  }

  setOpen(isOpen: boolean) {
    this.isAlertOpen = isOpen;
  }
}
